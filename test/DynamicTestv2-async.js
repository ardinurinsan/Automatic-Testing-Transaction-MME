const fs = require('fs');
const axios = require('axios');
const csv = require('csv-parser');
const ExcelJS = require('exceljs');

const results = [];
let data_json = [];

async function compareResponses(iterasi, url_1, url_2) {
    let response_url_1, response_url_2, data_1, data_2, result, data;
    try {
        response_url_1 = await axios.get(url_1);
        data_1 = response_url_1.data;
        response_url_2 = await axios.get(url_2);
        data_2 = response_url_2.data;
        result = data_1 === data_2 ? 'OK! Response Sama' : 'Not! Responses Tidak Sama';
        console.log(`Line ke-${iterasi} : ${result}`);
    } catch (error) {
        console.error('Error:', error.message);
    }

    data = {
        'Link Port Lama': url_1,
        'Link Port Baru': url_2,
        'Response Link Lama': data_1,
        'Response Link Baru': data_2,
        'Result': result
    };

    data_json.push(data);
    storeDataToExcel(data_json);
}

function storeDataToExcel(data) {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Sheet 1');

    worksheet.columns = [
        { header: 'Link Port Lama', key: 'Link Port Lama', width: 20 },
        { header: 'Link Port Baru', key: 'Link Port Baru', width: 10 },
        { header: 'Response Link Lama', key: 'Response Link Lama', width: 30 },
        { header: 'Response Link Baru', key: 'Response Link Baru', width: 30 },
        { header: 'Result', key: 'Result', width: 30 }
    ];

    data.forEach(item => {
        worksheet.addRow(item);
    });

    const notSameColor = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFF0000' },
        bgColor: { argb: 'FF0000FF' }
    };

    worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
        if (rowNumber > 1) {
            const resultCell = row.getCell('Result');
            if (resultCell.value === 'Not! Responses Tidak Sama') {
                resultCell.fill = notSameColor;
            }
        }
    });

    const sameColor = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FF00FF00' },
        bgColor: { argb: 'FF0000FF' }
    };
    worksheet.eachRow({ includeEmpty: false }, function (row, rowNumber) {
        if (rowNumber > 1) {
            const resultCell = row.getCell('Result');
            if (resultCell.value === 'OK! Response Sama') {
                resultCell.fill = sameColor;
            }
        }
    });

    workbook.xlsx.writeFile('../data/data-link-response-v2.xlsx')
        .then(() => {
        })
        .catch(err => {
            console.error('Error saving Excel file:', err);
        });
}

fs.createReadStream('../data/data-link-response-inquiry.csv')
    .pipe(csv({ separator: ';' }))
    .on('data', (data) => results.push(data))
    .on('end', () => {
        let line_iterasi = 2;
        for (let i = 0; i < results.length; i++) {
            compareResponses(line_iterasi++, results[i][Object.keys(results[i])[0]], results[i][Object.keys(results[i])[1]]);
        }
    });